//
//  Datasource.swift
//  WIDGETHAHA
//
//  Created by FAUZIA UMAR on 20/06/20.
//  Copyright © 2020 FAUZIA UMAR. All rights reserved.
//

import Foundation

struct DataStore {
    var nameOfThings: String
    var category: String
    
    init(name: String, category: String) {
        self.nameOfThings = name
        self.category = category
    }
}

class datamanager  {
    
    var arraydata: [DataStore]
    
    init() {
        arraydata = [
            DataStore(name: "banana", category: "fruit"),
            DataStore(name: "apple", category: "fruit"),
            DataStore(name: "peer", category: "fruit"),
            DataStore(name: "orange", category: "fruit"),
            DataStore(name: "papaya", category: "fruit"),
            DataStore(name: "brocoli", category: "vegetable"),
            DataStore(name: "tomato", category: "vegetable"),
            DataStore(name: "carrot", category: "vegetable"),
            DataStore(name: "chili", category: "vegetable"),
            DataStore(name: "cucumbar", category: "vegetable"),
            DataStore(name: "popcorn", category: "grains"),
            DataStore(name: "oat", category: "grains"),
            DataStore(name: "egg", category: "proteinfood"),
            DataStore(name: "beef", category: "proteinfood"),
            DataStore(name: "milk", category: "dairy"),
            DataStore(name: "yogurt", category: "dairy"),
        ]
    
    }
}

